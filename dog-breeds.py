import requests
import sys

API_URL = "https://dog.ceo/api"

def list_all_breeds():
    response = requests.get(f"{API_URL}/breeds/list/all")
    if response.status_code == 200:
        breeds = response.json().get('message', {})
        for breed, subbreeds in breeds.items():
            print(f"{breed}")
            for subbreed in subbreeds:
                print(f"  - {subbreed}")
    else:
        print("Failed to retrieve breeds list")

def get_image_breed(breed, file_path):
    response = requests.get(f"{API_URL}/breed/{breed}/images/random")
    if response.status_code == 200:
        image_url = response.json().get('message', None)
        download_image(image_url, file_path)
    else:
        print("Failed to retrieve image URL")

def get_image_subbreed(breed, subbreed, file_path):
    response = requests.get(f"{API_URL}/breed/{breed}/{subbreed}/images/random")
    if response.status_code == 200:
        image_url = response.json().get('message', None)
        download_image(image_url, file_path)
    elif response.status_code == 404:
        print(response.status_code, "Verify that subbreed belongs to breed category.")
    else:
        print("Failed to retrieve image URL")

def download_image(image_url, file_path):
    if image_url:
        img_response = requests.get(image_url)
        if img_response.status_code == 200:
            with open(file_path, 'wb') as f:
                f.write(img_response.content)
            print(f"Image saved as {file_path}")
        else:
            print("Failed to download the image")
    else:
        print("Image not found for the specified breed")

def main():
    if len(sys.argv) < 2:
        print("Usage:")
        print("$ python3 dog-breeds.py list-all")
        print("$ python3 dog-breeds.py get-image-breed --breed \"BREED_NAME\" --file \"./image.jpg\"")
        print("$ python3 dog-breeds.py get-image-subbreed --breed \"BREED_NAME\" --subbreed \"SUBBREED_NAME\" --file \"./image.jpg\"")
        return

    command = sys.argv[1]
    if command == "list-all":
        list_all_breeds()
    elif command == "get-image-breed":
        breed = None
        file_path = None
        if "--breed" in sys.argv:
            breed_index = sys.argv.index("--breed") + 1
            if breed_index < len(sys.argv):
                breed = sys.argv[breed_index]
        if "--file" in sys.argv:
            file_index = sys.argv.index("--file") + 1
            if file_index < len(sys.argv):
                file_path = sys.argv[file_index]
        if breed and file_path:
            get_image_breed(breed, file_path)
        else:
            print("Missing arguments for get-image-breed")
    elif command == "get-image-subbreed":
        breed = None
        file_path = None
        if "--breed" in sys.argv:
            breed_index = sys.argv.index("--breed") + 1
            if breed_index < len(sys.argv):
                breed = sys.argv[breed_index]
        if "--subbreed" in sys.argv:
            subbreed_index = sys.argv.index("--subbreed") + 1
            if subbreed_index < len(sys.argv):
                subbreed = sys.argv[subbreed_index]
        if "--file" in sys.argv:
            file_index = sys.argv.index("--file") + 1
            if file_index < len(sys.argv):
                file_path = sys.argv[file_index]
        if breed and file_path:
            get_image_subbreed(breed, subbreed, file_path)
        else:
            print("Missing arguments for get-image-subbreed")
    else:
        print("Unknown command")

if __name__ == "__main__":
    main()
