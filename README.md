# Dog Breeds - Dog Image CLI

Dog Breeds CLI is a simple command-line interface (CLI) tool that interacts with the [Dog CEO's Dog API](https://dog.ceo/dog-api/) to list all dog breeds and download random images for a specific breed or sub-breed.

## Features

- List all dog breeds and sub-breeds.
- Download a random image of a specific dog breed.
- Download a random image of a specific sub-breed.

## Prerequisites

Before you begin, ensure you have met the following requirements:
- You have a `Linux` or `Windows` machine.
- You have installed Python 3.x.

## Installation

To install Dog Breeds, follow these steps:

Linux and macOS:

```
git clone git@gitlab.com:nachiketas/cli_tool.git
cd cli_tool
```

Windows:

```
git clone git@gitlab.com:nachiketas/cli_tool.git
cd cli_tool
```

## Usage
To use Dog Breeds, you can execute the script directly with Python. Here are some usage examples:

List all breeds:
```
$ python3 dog-breeds.py list-all
```

Download a random image for a specific breed:
```
$ python3 dog-breeds.py get-image-breed --breed "labrador" --file "./labrador.jpg"
```

Download a random image for a specific sub-breed:
```
$ python3 dog-breeds.py get-image-subbreed --breed "bulldog" --subbreed "boston" --file "./boston_bulldog.jpg"
```

## Compilation
To create an exec file, follow the steps:
1) pip install pyinstaller
2) pyinstaller --onefile dog-breeds.py

After the process completes, you'll find the dog_breeds executable in the dist directory that PyInstaller creates.

For ease of use, you may want to move the executable to a directory in your PATH, like /usr/local/bin:
```
sudo mv ./dist/dog-breeds /usr/local/bin/dog-breeds
```

Now you can run dog-breeds from anywhere in your shell:
```
dog-breeds list-all
dog-breeds get-image --breed "labrador" --file "./image.jpg"
```

## Contribution
Contributions to the Dog Breeds project are welcome!

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".

Don't forget to give the project a star! Thanks again!

1) Create your Feature Branch from dev branch (git checkout -b YourName/AmazingFeature)
2) Do your changes and Add them (git add <ModifiedFile>)
3) Commit your Changes respecting the rules of contributing (git commit -s)
4) Push your branch  (git push origin feature/AmazingFeature)
5) Open a Pull Request onto Dev Branch


## Contact

Sean McGannon: smcgannon@engit.fr
Project Link: https://gitlab.com/nachiketas/cli_tool.git


## Acknowledgments

Dog CEO's Dog API
